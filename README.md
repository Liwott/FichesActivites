# FichesActivites

:fr:
Package LaTeX pour créer des fiches activités dans le contexte de l'animation en plaine de jeux.

La branche `example` de ce dépôt contient un exemple d'usage.

:uk:
LaTeX package to create activity sheets ("fiches activités") in the context of animation in summer camps.

The `example` branch of this repository contains an usage example.